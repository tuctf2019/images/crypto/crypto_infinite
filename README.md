# Crypto Infinite -- Crypto -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/crypto/crypto_infinite)

## Chal Info

Desc: `Crypto has been around forever. Does this challenge go on forever? Let us know in the comments below or find the flag to prove us wrong.`

Flag: `TUCTF{1nf1n1t3_1s_n0t_4_g00d_n4m3}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/crypto_infinite)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/crypto_infinite:tuctf2019
```

