
import sys

class PigPen(object):

    PIGPEN_DICT = {
        'A':'_|',
        'B':'|_|',
        'C':'|_',
        'D':']',
        'E':'[]',
        'F':'[',
        'G':'-|',
        'H':'|-|',
        'I':'|-',
        'J':'._|',
        'K':'|._|',
        'L':'|._',
        'M':'.]',
        'N':'[.]',
        'O':'[.',
        'P':'.-|',
        'Q':'|.-|',
        'R':'|.-',
        'S':'v',
        'T':'>',
        'U':'<',
        'V':'^',
        'W':'.v',
        'X':'.>',
        'Y':'.<',
        'Z':'.^',
        '.':'\n',
        ' ':'   '
    }

    PIGPEN_ROTATE_RIGHT_DICT = {
        # A through I
        '_|'  : '|_|',
        '|_|' : '|_',
        '|_'  : '[',
        '['   : '|-',
        '|-'  : '|-|',
        '|-|' : '-|',
        '-|'  : ']',
        ']'   : '_|',
        '[]'  : '[]',

        # J through R
        '._|'  : '|._|',
        '|._|' : '|._',
        '|._'  : '[.',
        '[.'   : '|.-',
        '|.-'  : '|.-|',
        '|.-|' : '.-|',
        '.-|'  : '.]',
        '.]'   : '._|',
        '[.]'  : '[.]',

        # S through V
        'v'    : '<',
        '<'    : '^',
        '^'    : '>',
        '>'    : 'v',

        # W through Z
        '.v'   : '.<',
        '.<'   : '.^',
        '.^'   : '.>',
        '.>'   : '.v',

        # misc
        '\n'   : '\n',
        '   '  : '   '
    }

    SYMBOLS = ['_', '|', ']', '[', '-', '.', 'v', '>', '<', '^']

    if sys.version_info >= (3, 0):
        PIGPEN_REV_DICT = {v: k for k, v in PIGPEN_DICT.items()}
        PIGPEN_ROTATE_LEFT_DICT = {v: k for k, v in PIGPEN_ROTATE_RIGHT_DICT.items()}
    else:
        PIGPEN_REV_DICT = {v: k for k, v in PIGPEN_DICT.iteritems()}
        PIGPEN_ROTATE_LEFT_DICT = {v: k for k, v in PIGPEN_ROTATE_RIGHT_DICT.iteritems()}

    pigpen = []

    def __init__(self, pigpen):
        self.pigpen = pigpen

    @classmethod
    def encipher(cls, string):
        return cls([cls.PIGPEN_DICT[c] for c in string])

    @classmethod
    def from_pigpen_string(cls, ppstring):
        as_words = ppstring.strip('\n').split('   ')
        if len(as_words) > 1:
            pp = []
            for word in as_words:
                pp.extend(word.split(' '))
                pp.append('    ')
            return cls(pp[:-1])
        return cls(as_words[0].split(' '))

    def __str__(self):
        return ' '.join(self.pigpen)

    def __len__(self):
        return len(self.pigpen)

    def decipher(self):
        return ''.join([self.PIGPEN_REV_DICT[p] for p in self.pigpen])

    def rotate_right(self):
        self.pigpen = [self.PIGPEN_ROTATE_RIGHT_DICT[p] for p in self.pigpen]
        return self

    def rotate_left(self):
        self.pigpen = [self.PIGPEN_ROTATE_LEFT_DICT[p] for p in self.pigpen]
        return self

    def rotate_symbols_right(self):
        pp = []
        for p in self.pigpen:
            pp.append(''.join([self.SYMBOLS[(self.SYMBOLS.index(c) + 1) % len(self.SYMBOLS)] for c in p]))
        self.pigpen = pp
        return self

    def rotate_symbols_left(self):
        pp = []
        l = len(self.SYMBOLS) - 1
        for p in self.pigpen:
            pp.append(''.join([self.SYMBOLS[(self.SYMBOLS.index(c) + l) % len(self.SYMBOLS)] for c in p]))
        self.pigpen = pp
        return self
